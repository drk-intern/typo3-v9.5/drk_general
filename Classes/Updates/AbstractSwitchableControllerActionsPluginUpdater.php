<?php
namespace DRK\DrkGeneral\Updates;

/**
 * This Class is heavily inspired by the work of @derhansen
 *
 * Source:
 *   * https://gist.github.com/derhansen/4524495ccfef9335c96d6d535bad7324
 *
 * Article with more details:
 *   * https://www.derhansen.de/2021/03/migrate-switchablecontrolleractions.html
 * Migration of existing plugins
 *
 * To be able to migrate all existing plugins and settings to the new plugins,
 * he created a custom upgrade wizard that takes care of all required tasks. Those tasks are as following:
 *
 * * Determine, which tt_content record need to be updated
 * * Analyse existing Plugin (field: list_type) and switchableControllerActions in FlexForm (field: pi_flexform)
 * * Remove non-existing settings and switchableControllerAction from FlexForm by comparing settings with new FlexForm structure of target plugin
 * * Update tt_content record with new Plugin and FlexForm
 *
 * As a result, a SwitchableControllerActionsPluginUpdater has been added to the extension. It takes care of all mentioned tasks and has a configuration array which contains required settings (source plugin, target plugin and switchableControllerActions) for the migration.
 *
 * To use this class extend from it and set the constants to meaningful values
 * * MIGRATION_SETTINGS constant
 * * DESCRIPTION
 *
 * Example
 *
 * namespace DRK\DrkGeneral\Updates;
 *
 * use TYPO3\CMS\Install\Attribute\UpgradeWizard;
 *
 * #[UpgradeWizard('drkgeneral_exampleUpdateWizard')]
 * class ExampleUpdateWizard extends AbstractSwitchableControllerActionsPluginUpdater
 * {
 *     private const TITLE = 'Meaningful title';
 *     private const DESCRIPTION = 'Meaningful description';
 *     private const MIGRATION_SETTINGS = [
 *         //...
 *     ];
 * }
 *
 */

use Doctrine\DBAL\ArrayParameterType;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\ChattyInterface;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;

abstract class AbstractSwitchableControllerActionsPluginUpdater implements UpgradeWizardInterface, ChattyInterface
{
    protected const MIGRATION_SETTINGS = [
        [
            'sourceListType' => 'plainfaq_pi1',
            'switchableControllerActions' => 'Faq->list;Faq->detail',
            'targetListType' => 'plainfaq_pilistdetail',
            'targetCtype' => ''
        ],
        [
            'sourceListType' => 'plainfaq_pi1',
            'switchableControllerActions' => 'Faq->list',
            'targetListType' => 'plainfaq_pilist'
        ],
        [
            'sourceListType' => 'plainfaq_pi1',
            'switchableControllerActions' => 'Faq->detail',
            'targetListType' => 'plainfaq_pidetail'
        ],
    ];

    protected FlexFormService $flexFormService;

    protected OutputInterface $output;

    public function __construct()
    {
        $this->flexFormService = GeneralUtility::makeInstance(FlexFormService::class);
    }

    /**
     * Setter injection for output into upgrade wizards
     */
    public function setOutput(OutputInterface $output): void
    {
        $this->output = $output;
    }

    public function getTitle(): string
    {
        $title = [
            '[drk_template]: Migration switchableControllerActions'
        ];
        foreach (static::MIGRATION_SETTINGS as $setting)
        {

            $title[] = $setting['sourceListType'];
            $title[] = 'with actions';
            $title[] = $setting['switchableControllerActions'];
            $title[] = 'to list';
            $title[] = $setting['targetListType'];
            $title[] = 'to ctype';
            $title[] = $setting['targetCtype'];
            $title[] = ',';
        }

        return implode(' ', $title);
    }

    public function getDescription(): string
    {
        return 'This wizard migrates Switchable controller actions into real plugins';
    }

    public function getPrerequisites(): array
    {
        return [
            DatabaseUpdatedPrerequisite::class
        ];
    }

    public function updateNecessary(): bool
    {
        return $this->checkIfWizardIsRequired();
    }

    public function executeUpdate(): bool
    {
        return $this->performMigration();
    }

    public function checkIfWizardIsRequired(): bool
    {
        return count($this->getMigrationRecords()) > 0;
    }

    public function performMigration(): bool
    {
        $records = $this->getMigrationRecords();

        foreach ($records as $record) {
            $flexFormData = GeneralUtility::xml2array($record['pi_flexform']);
            $flexForm = $this->flexFormService->convertFlexFormContentToArray($record['pi_flexform']);

            if (!isset($flexForm['switchableControllerActions'])) {
                continue;
            }

            $targetListType = $this->getTargetListType(
                $record['list_type'],
                $flexForm['switchableControllerActions']
            );
            $targetCtype = $this->getTargetCType(
                $record['list_type'],
                $flexForm['switchableControllerActions']
            );

            $allowedSettings = [];

            if ($targetListType !== '') {
                $allowedSettings = $this->getAllowedSettingsFromFlexForm($targetListType, '', ',list');
            }
            if ($targetCtype !== '') {
                $allowedSettings = $this->getAllowedSettingsFromFlexForm($targetCtype, '*,', '');
            }

            // Remove flexform data which do not exist in flexform of new plugin
            foreach ($flexFormData['data'] as $sheetKey => $sheetData) {
                if (is_array($sheetData['lDEF'] )) {
                    foreach ($sheetData['lDEF'] as $settingName => $setting) {
                        if (!in_array($settingName, $allowedSettings, true)) {
                            unset($flexFormData['data'][$sheetKey]['lDEF'][$settingName]);
                        }
                    }

                    // Remove empty sheets
                    if (!count($flexFormData['data'][$sheetKey]['lDEF']) > 0) {
                        unset($flexFormData['data'][$sheetKey]);
                    }
                }
            }

            if (count($flexFormData['data']) > 0) {
                $newFlexform = $this->array2xml($flexFormData);
            } else {
                $newFlexform = '';
            }

            $this->updateContentElement(
                $record['uid'],
                $newFlexform,
                $targetListType,
                $targetCtype,
            );

            $this->output->writeln(
                implode(
                    ' ',
                    [
                        'Updated records',
                        'uid:',
                        $record['uid'],
                        'Listtype:',
                        $targetListType,
                        'CType:',
                        $targetCtype,
                    ]
                )

            );
        }

        return true;
    }

    protected function getMigrationRecords(): array
    {
        $checkListTypes = array_unique(array_column(static::MIGRATION_SETTINGS, 'sourceListType'));

        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tt_content');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        return $queryBuilder
            ->select('uid', 'list_type', 'pi_flexform')
            ->from('tt_content')
            ->where(
                $queryBuilder->expr()->in(
                    'list_type',
                    $queryBuilder->createNamedParameter($checkListTypes, ArrayParameterType::STRING)
                )
            )
            ->executeQuery()
            ->fetchAllAssociative();
    }

    protected function getTargetListType(string $sourceListType, string $switchableControllerActions): string
    {
        foreach (static::MIGRATION_SETTINGS as $setting) {
            if ($setting['sourceListType'] === $sourceListType &&
                $setting['switchableControllerActions'] === $switchableControllerActions &&
                isset($setting['targetListType'])
            ) {
                return $setting['targetListType'];
            }
        }

        return '';
    }

    protected function getTargetCType(string $sourceListType, string $switchableControllerActions): string
    {
        foreach (static::MIGRATION_SETTINGS as $setting) {
            if ($setting['sourceListType'] === $sourceListType &&
                $setting['switchableControllerActions'] === $switchableControllerActions &&
                isset($setting['targetCtype'])
            ) {
                return $setting['targetCtype'];
            }
        }

        return '';
    }

    protected function getAllowedSettingsFromFlexForm(string $listType, string $prepend = '', string $append = ',list'): array
    {

        $settings = [];
        // check if pi_flexform config exists
        if (
            !isset($GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][$prepend . $listType . $append])
        ) {
            return $settings;
        }

        $flexFormFile = $GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][$prepend . $listType . $append];
        $flexFormContent = file_get_contents(GeneralUtility::getFileAbsFileName(substr(trim($flexFormFile), 5)));
        $flexFormData = GeneralUtility::xml2array($flexFormContent);

        // If sheets exists, iterate each sheet and extract all settings
        if (!empty($flexFormData['sheets']) && is_array($flexFormData['sheets'])) {
            foreach ($flexFormData['sheets'] as $sheet) {
                foreach ($sheet['ROOT']['el'] as $setting => $tceForms) {
                    $settings[] = $setting;
                }
            }
        }
        return $settings;
    }

    /**
     * Updates list_type and pi_flexform of the given content element UID
     *
     * @param int $uid
     * @param string|null $newListType
     * @param string|null $flexform
     */
    protected function updateContentElement(int $uid, string $flexform, string $newListType = null, string $newCtype = null): void
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tt_content');

        if (($newListType !== null) && ($newListType !== '')) {
            $this->output->writeln('set new list type ' . $newCtype);
            $queryBuilder->update('tt_content')
                ->set('list_type', $newListType)
                ->set('pi_flexform', $flexform)
                ->where(
                    $queryBuilder->expr()->in(
                        'uid',
                        $queryBuilder->createNamedParameter($uid, Connection::PARAM_INT)
                    )
                )
                ->executeStatement();
        }
        if (($newCtype !== null) && ($newCtype !== '')) {
            $this->output->writeln('set new ctype ' . $newCtype);
            $queryBuilder->update('tt_content')
                ->set('CType', $newCtype)
                ->set('pi_flexform', $flexform)
                ->where(
                    $queryBuilder->expr()->in(
                        'uid',
                        $queryBuilder->createNamedParameter($uid, Connection::PARAM_INT)
                    )
                )
                ->executeStatement();
        }
    }

    /**
     * Transforms the given array to FlexForm XML
     *
     * @param array $input
     * @return string
     */
    protected function array2xml(array $input = []): string
    {
        $options = [
            'parentTagMap' => [
                'data' => 'sheet',
                'sheet' => 'language',
                'language' => 'field',
                'el' => 'field',
                'field' => 'value',
                'field:el' => 'el',
                'el:_IS_NUM' => 'section',
                'section' => 'itemType'
            ],
            'disableTypeAttrib' => 2
        ];
        $spaceInd = 4;
        $output = GeneralUtility::array2xml($input, '', 0, 'T3FlexForms', $spaceInd, $options);
        $output = '<?xml version="1.0" encoding="utf-8" standalone="yes" ?>' . LF . $output;
        return $output;
    }
}
