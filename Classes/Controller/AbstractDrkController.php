<?php

namespace DRK\DrkGeneral\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Http\Stream;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\RequestInterface;

class AbstractDrkController extends ActionController
{
    /**
     * @var array
     */
    public $error = [];

    /**
     * @var array
     */
    protected $mandatoryFormFields = [];


    /**
     * Merge FlexForms configuration and TypoScript settings
     */
    protected function initializeAction(): void
    {
        parent::initializeAction();
        if (isset($this->settings['flexforms']) && is_array($this->settings['flexforms'])) {
            $flexFormsSettingsArray = $this->settings['flexforms'];
            unset($this->settings['flexforms']);
            ArrayUtility::mergeRecursiveWithOverrule($this->settings, $flexFormsSettingsArray, true, false);
        }
    }

    /**
     * @param string $url
     * @return bool
     */
    public static function validateUrl(string $url): bool
    {
        preg_match('#(?<scheme>[a-z]+://)?(?<rest>.+)#', $url, $matches);
        if (empty($matches['scheme'])) {
            // assume a scheme
            $url = sprintf('https://%s', $url);
        }

        return false !== filter_var($url, FILTER_VALIDATE_URL)
            || false !== filter_var(
                (string)idn_to_ascii($url, IDNA_NONTRANSITIONAL_TO_ASCII, INTL_IDNA_VARIANT_UTS46),
                FILTER_VALIDATE_URL
            );
    }

    /**
     * @param string $email
     * @return bool
     */
    public static function isValidEmail(string $email): bool
    {
        preg_match('/(?<local>[^@]+)@(?<domain>[^@]+)/', $email, $matches);
        $idnEmail = sprintf(
            '%s@%s',
            $matches['local'],
            idn_to_ascii($matches['domain'], IDNA_NONTRANSITIONAL_TO_ASCII, INTL_IDNA_VARIANT_UTS46)
        );

        return false !== filter_var($email, FILTER_VALIDATE_EMAIL, FILTER_FLAG_EMAIL_UNICODE)
            || false !== filter_var($idnEmail, FILTER_VALIDATE_EMAIL, FILTER_FLAG_EMAIL_UNICODE);
    }

    /**
     * @param $mandatoryFields
     * @param $fields
     *
     * @return bool
     */
    protected function checkMandatoryFormFields($mandatoryFields, $fields): bool
    {
        foreach ($mandatoryFields as $mandatoryField) {
            if (isset($fields[$mandatoryField]) && empty($fields[$mandatoryField])) {
                $this->error['Pflichtfelder'] = 'Bitte füllen sie alle mit einem Stern (*) gekennzeichneten Felder aus.';

                return false;
            } elseif ($mandatoryField == 'email' && !$this->isValidEmail($fields[$mandatoryField])) {
                $this->error['Pflichtfelder'] = 'Eine mit einem Stern (*) gekennzeichneten Mailadresse ist nicht korrekt.';
                return false;
            }
        }

        return true;
    }

    /**
     * isHoneypotFilled
     *
     * @param $honeypotField
     * @return bool
     */
    protected function isHoneypotFilled($honeypotField): bool
    {
        if (!empty(trim($honeypotField)))
        {
            $this->error['Spamprotection'] = 'You are a spambot!';
            return false;
        }

        return true;

    }

    /**
     * Catch unpredictable SOAP errors in the ActionMethod
     *
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    protected function callActionMethod(\TYPO3\CMS\Extbase\Mvc\RequestInterface $request): \Psr\Http\Message\ResponseInterface
    {
        try {
            return parent::callActionMethod($request);
        } catch (\SoapFault|\RuntimeException|\Exception $e) {
            $this->view->assign('error', 'General Error: ' . $e->getMessage());

            $body = new Stream('php://temp', 'rw');
            $body->write('<p><b>Fehler:</b> Der Webservice antwortet nicht oder fehlerhaft!</p>');

            $response = new Response();
            $body->rewind();
            $response = $response->withBody($body);

            GeneralUtility::makeInstance(\TYPO3\CMS\Core\Log\LogManager::class)
                ->getLogger(__CLASS__)
                ->error($e->getMessage());
            return $response;
        }
    }
}
