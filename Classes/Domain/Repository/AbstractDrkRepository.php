<?php

namespace DRK\DrkGeneral\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Exception;
use SoapClient;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use DRK\DrkGeneral\Service\JsonClient;
use DRK\DrkGeneral\Service\JsonClientOld;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use TYPO3\CMS\Core\Cache\Frontend\VariableFrontend;

class AbstractDrkRepository
{
    /**
     * @var SoapClient
     */
    protected $courseClient = null;

    /**
     * @var SoapClient
     */
    protected $dldbClient = null;

    /**
     * @var SoapClient
     */
    protected $typo3Client = null;

    /**
     * @var JsonClient
     */
    protected $jsonClient = null;

    /**
     * @var array settings
     */
    protected $settings = [];

    /**
     * @var array error
     * public error array
     */
    public array $error = [];

    /**
     * cache time 60s
     * @var int
     */
    protected int $short_cache = 60;
    /**
     * cache time 1 hour
     * @var int
     */
    protected int $long_cache = 3600;
    /**
     * cache time 1 day
     * @var int
     */
    protected int $heavy_cache = 86400;

    /**
     * @param array|null $settings
     */
    public function __construct(array $settings = null)
    {
        if ($settings !== null) {
            $this->setSettings($settings);
        }
        $this->initSettings();
    }

    protected ?VariableFrontend $cache = null;

    /**
     * @todo refactor to use DI, fails because of constructors for now
     *
     * @return FrontendInterface|VariableFrontend|null
     * @throws \TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException
     */
    public function getCache(): VariableFrontend|FrontendInterface|null
    {
        if (is_null($this->cache)) {
            $cm = GeneralUtility::makeInstance(CacheManager::class);
            $this->cache = $cm->getCache('drk_general_apicache');
        }
        return $this->cache;
    }

    /**
     * @return SoapClient
     * @throws Exception
     */
    protected function getDldbClient(): SoapClient
    {
        return $this->getClient('dldb');
    }

    /**
     * @return SoapClient
     * @throws Exception
     */
    protected function getCourseClient(): SoapClient
    {
        return $this->getClient('course');
    }

    /**
     * @return SoapClient
     * @throws Exception
     */
    protected function getTypo3Client(): SoapClient
    {
        return $this->getClient('typo3');
    }

    /**
     * @param $type
     *
     * @return SoapClient
     * @throws Exception
     */
    protected function getClient($type): SoapClient
    {
        $wsdl = $this->settings['ws_' . $type . '_wsdl'] ?? null;
        if (empty($wsdl)) {
            throw new Exception('No endpoint defined', 1452510309186);
        }
        $client = $this->{$type . 'Client'};
        if ($client === null) {
            $soapClientOptions = [
                'stream_context' => stream_context_create(
                    array(
                        'http' => array(
                            'user_agent' => 'PHPSoapClient',
                            "timeout" => 10,
                        )
                    )
                ),
                'soap_version' => SOAP_1_1,
                'encoding' => 'UTF-8'
            ];

            if (!empty($this->settings['ws_auth_login']) && !empty($this->settings['ws_auth_password'])) {
                $soapClientOptions['login'] = $this->settings['ws_auth_login'];
                $soapClientOptions['password'] = $this->settings['ws_auth_password'];
            }
            try {
                $client = $this->{$type . 'Client'} = new SoapClient($wsdl, $soapClientOptions);
            } catch (\SoapFault $e) {
                GeneralUtility::makeInstance(\TYPO3\CMS\Core\Log\LogManager::class)
                    ->getLogger(__CLASS__)
                    ->error($e->getMessage());
                $this->error = ['Error' => $e->getMessage()];
            }
        }

        if ($client === null) {
            throw new Exception('Could not create client for:' . $wsdl, 1452510309186);
        }

        return $client;
    }

    /**
     *
     * @param string $sVersion
     *
     * @throws Exception
     */
    protected function initJsonClient(string $sVersion = ''): void
    {
        $serviceUrl = trim($this->settings['serviceUrl']);
        if (empty($serviceUrl)) {
            throw new Exception('URL des Webservice fehlt!', 1452510309186);
        }

        if (!isset($this->settings['apiKey']) || empty(trim($this->settings['apiKey']))) {
            throw new Exception('API-Schlüssel fehlt! Bitte tragen Sie diesen nach.', 1452510309186);
        }

        if ($sVersion == 'old') {
            $this->jsonClient = GeneralUtility::makeInstance(JsonClientOld::class, $serviceUrl);
        } else {
            $this->jsonClient = GeneralUtility::makeInstance(JsonClient::class, $serviceUrl);
        }
    }

    /**
     * @param string $sVersion
     *
     * @return JsonClient|null
     * @throws Exception
     */
    public function getJsonClient(string $sVersion = ''): JsonClient|JsonClientOld
    {
        if (empty($this->jsonClient)) {
            $this->initJsonClient($sVersion);
        }
        return $this->jsonClient;
    }

    /**
     * @param $action
     * @param array $request
     *
     * @return array|bool|null
     * @throws Exception
     */
    public function executeJsonClientAction($action, array $request = []): bool|array|null
    {
        if (!is_string($action)) {
            $this->error = ['Error' => 'Invalid Action!'];
            return false;
        }

        if (empty($request)) {
            $this->error = ['Error' => 'No object to send!'];
            return false;
        }

        $this->getJsonClient();
        $response = $this->jsonClient->execute($action, $request);

        if (!empty($response)) {
            if ($response['status'] != "OK") {
                $this->error = ['Error' => "Der Webservice meldet: " . $response['message']];
            } else {
                return $response['response'];
            }
        } else {
            $this->error = ['Error' => 'Webservice antwortet nicht oder fehlerhaft!'];
        }

        return false;
    }

    /**
     * Get current TypoScript settings
     *
     * @param null $type
     */
    protected function initSettings($type = null): void
    {
        if (!empty($this->getSettings())) {
            return;
        }

        switch ($type) {
            case 'FRAMEWORK':
                $configurationType = ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK;
                break;

            case 'FULL_TYPOSCRIPT':
                $configurationType = ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT;
                break;

            case 'SETTINGS':
            default:
                $configurationType = ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS;
                break;
        }

        /** @var ConfigurationManagerInterface $configurationManager */
        $configurationManager = GeneralUtility::makeInstance(ConfigurationManagerInterface::class);
        $this->setSettings($configurationManager->getConfiguration($configurationType));
    }

    /**
     * @param array $parameters
     *
     */
    protected function initSettingsForListItems(array $parameters): void
    {
        if (empty($parameters['config']['itemsProcFunc_params'])
            || empty($parameters['config']['itemsProcFunc_params']['extensionName'])
            || empty($parameters['config']['itemsProcFunc_params']['pluginName'])
        ) {
            throw new \InvalidArgumentException('Missing itemsProcFunc configuration.', 1455789129533);
        }

        if (empty((int)GeneralUtility::_GP('id'))) {
            $_GET['id'] = $parameters['flexParentDatabaseRow']['pid'];
        }

        $configuration = $parameters['config']['itemsProcFunc_params'];
        /** @var ConfigurationManagerInterface $configurationManager */
        $configurationManager = GeneralUtility::makeInstance(ConfigurationManagerInterface::class);
        $configurationManager->setConfiguration($configuration);
        $setup = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

        // Get current TypoScript settings
        $settings = [];
        $typoScriptService = GeneralUtility::makeInstance(TypoScriptService::class);
        if (isset($setup['plugin.']['tx_' . strtolower($configuration['extensionName']) . '.']['settings.']) &&
            is_array($setup['plugin.']['tx_' . strtolower($configuration['extensionName']) . '.']['settings.'])) {
            $settings = $typoScriptService->convertTypoScriptArrayToPlainArray(
                $setup['plugin.']['tx_' . strtolower($configuration['extensionName']) . '.']['settings.']
            );
        }
        $pluginSignature = strtolower($configuration['extensionName'] . '_' . $configuration['pluginName']);
        if (isset($setup['plugin.']['tx_' . $pluginSignature . '.']['settings.']) &&
            is_array($setup['plugin.']['tx_' . $pluginSignature . '.']['settings.'])) {
            ArrayUtility::mergeRecursiveWithOverrule(
                $settings,
                $typoScriptService->convertTypoScriptArrayToPlainArray(
                    $setup['plugin.']['tx_' . $pluginSignature . '.']['settings.']
                )
            );
        }
        $this->setSettings($settings);
    }

    /**
     * @return array
     */
    public function getSettings(): array
    {
        return $this->settings;
    }

    /**
     * @param $settings
     */
    public function setSettings($settings): void
    {
        $this->settings = $settings;
    }

    /**
     * @return array|bool
     */
    public function getErrors(): bool|array
    {
        return $this->error;
    }
}
