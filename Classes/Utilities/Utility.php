<?php

namespace DRK\DrkGeneral\Utilities;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
class Utility
{
    public static $prefixArray = [29 => '', 1 => 'Herr', 2 => 'Frau'];

    public static $titleArray = [0 => '', 1 => 'Dr.', 2 => 'Prof.'];

    protected static $daysOfWeek = [
        'short' => ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
        'long' => ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag']
    ];

    /**
     * @param $sourceObject
     *
     * @return array
     */
    public static function convertObjectToArray($sourceObject)
    {
        $targetArray = null;
        if (is_object($sourceObject)) {
            $sourceObject = (array)$sourceObject;
        }
        if (is_array($sourceObject)) {
            $targetArray = array();
            foreach ($sourceObject as $key => $val) {
                $targetArray[$key] = self::convertObjectToArray($val);
            }
        } else {
            $targetArray = $sourceObject;
        }
        return $targetArray;
    }

    /**
     * @param array $data
     */
    public static function sortCourseArrayByDate(array &$data)
    {
        usort($data, function ($a, $b) {
            $aStartTime = strtotime($a['CourseDates'][0]['StartDate']);
            $bStartTime = strtotime($b['CourseDates'][0]['StartDate']);
            return ($aStartTime === $bStartTime) ? 0 : (($aStartTime < $bStartTime) ? -1 : 1);
        });
    }

    /**
     * @param array $course
     *
     * @param bool $showDayOfWeek
     *
     * @return array
     */
    public static function generateCourseDates(array $course, $showDayOfWeek = true)
    {
        $courseDateArray = [];
        if (is_array($course['CourseDates']) && !empty($course['CourseDates'])) {
            foreach ($course['CourseDates'] as $courseDateTempKey => $courseDateTemp) {
                $startDateTimestamp = strtotime($courseDateTemp['StartDate']);
                $courseDateArray[$courseDateTempKey]['date'] =
                    ($showDayOfWeek ? self::$daysOfWeek['short'][date('w', $startDateTimestamp)] . '. ' : '') .
                    date('d.m.Y', $startDateTimestamp);

                $StartTimeTemp = date('G:i', $startDateTimestamp);
                $EndTimeTemp = date('G:i', strtotime($courseDateTemp['EndDate']));

                $courseDateArray[$courseDateTempKey]['time'] = $StartTimeTemp . " - " . $EndTimeTemp;
            }
        }
        return $courseDateArray;
    }
}
