<?php

namespace DRK\DrkGeneral\Service\JsonRPC;

use \RuntimeException;
use \BadFunctionCallException;

/*
                    COPYRIGHT

Copyright 2007 Sergio Vaccaro <sergio@inservibile.org>

This file is part of JSON-RPC PHP.

JSON-RPC PHP is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

JSON-RPC PHP is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with JSON-RPC PHP; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/**
 * The object of this class are generic jsonRPC 1.0 clients for DRK-Services
 * http://json-rpc.org/wiki/specification
 *
 * @author sergio <jsonrpcphp@inservibile.org>
 */
class ClientOld
{

    /**
     * Debug state
     *
     * @var boolean
     */
    private bool $debug;

    /**
     * The server URL
     *
     * @var string
     */
    private string $url;
    /**
     * The request id
     *
     * @var integer
     */
    private int $id;
    /**
     * If true, notifications are performed instead of requests
     *
     * @var boolean
     */
    private bool $notification = false;

    /**
     * Takes the connection parameters
     *
     * @param string $url
     * @param boolean $debug
     */
    public function __construct(string $url, bool $debug = false)
    {
        // server URL
        $this->url = $url;
        // debug state
        empty($debug) ? $this->debug = false : $this->debug = true;
        // message id
        $this->id = 1;
    }

    /**
     * Sets the notification state of the object. In this state, notifications are performed, instead of requests.
     *
     * @param boolean $notification
     */
    public function setRPCNotification(bool $notification): void
    {
        empty($notification) ?
            $this->notification = false
            :
            $this->notification = true;
    }

    /**
     * Performs a jsonRCP request and gets the results as an array
     *
     * @param string $method
     * @param array $params
     * @return array
     */
    public function __call(string $method, array $params)
    {

        // check
        if (!is_scalar($method)) {
            throw new \BadFunctionCallException('Method name has no scalar value');
        }

        // check
        if (!is_array($params[0]) && !empty($params[0])) {
            throw new \BadFunctionCallException('Params must be given as array');
        }

        // sets notification or request task
        if ($this->notification) {
            $currentId = null;
        } else {
            $currentId = $this->id;
        }

        // prepares the request
        $request = '';
        foreach ($params[0] as $k => $v) {
            $request .= $k . '=' . $v . '&';
        }

        if (!empty($request)) {
            $call = $this->url . '/' . $method . '?' . $request;
        } else {
            $call = $this->url . '/' . $method;
        }

        $this->debug && $this->debug .= '***** Request *****' . "\n" . $request . "\n" . '***** End Of request *****' . "\n\n";

        if ($fp = fopen($call, 'r', false)) {
            $raw_response = '';
            while ($row = fgets($fp)) {
                $raw_response .= trim($row) . "\n";
            }
            $this->debug && $this->debug .= '***** Server response *****' . "\n" . $raw_response . '***** End of server response *****' . "\n";
            $response = json_decode($raw_response, false);
        } else {
            throw new \RuntimeException('Unable to connect to ' . $this->url);
        }

        // debug output
        if ($this->debug) {
            echo nl2br($this->debug);
        }

        // final checks and return
        if (!$this->notification) {
            // check
            if ($response->status != 'ok') {
                throw new \RuntimeException('Request error: ' . $response->status);
            }

            return $response;
        } else {
            return [];
        }
    }
}
