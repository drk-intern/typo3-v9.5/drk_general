<?php

namespace DRK\DrkGeneral\Service;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use DRK\DrkGeneral\Service\JsonRPC\ClientOld;

/**
 * Adapter for JsonRPC to be used by TYPO3 extensions.
 *
 * This will use the setting in TYPO3_CONF_VARS to choose the correct transport
 * for it to work out-of-the-box.
 *
 * @package DRK\DrkGeneral\Service
 *
 */
class JsonClientOld extends ClientOld
{

    /**
     * @var \Server URL
     */
    private string $url;

    /**
     * When constructing, also initializes the JsonRPC
     *
     * @param string $url
     *
     * @throws \Exception
     */
    public function __construct($url = "")
    {
        if (empty($url)) {
            throw new \Exception("URL des Webservice ist leer!");
        }
        $this->url = $url;

        /* todo set parameter from ts */
        parent::__construct($this->url);
    }
}
