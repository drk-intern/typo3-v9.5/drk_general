# TYPO3 Extension DRK General EXT:drk_general

Enthält zentrale Funktion für die TYPO3 DRK Extensions

## Methoden zum Zugriff auf die DRK DLDB und Co

Siehe `Classes/Service Verzeichnis`

## Vorlage für UpgradeWizards von SwitchableControllerActions

```php
<?php
namespace DRK\DrkGeneral\Updates;

use TYPO3\CMS\Install\Attribute\UpgradeWizard;

#[UpgradeWizard('drkgeneral_exampleUpdateWizard')]
class ExampleUpdateWizard extends AbstractSwitchableControllerActionsPluginUpdater
{
    private const TITLE = 'Meaningful title';
    private const DESCRIPTION = 'Meaningful description';
    private const MIGRATION_SETTINGS = [
        //...
    ];
}
```
